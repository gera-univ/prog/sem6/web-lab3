let data = null;
let formInfo = null;

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function loadData()
{
  try {
    data = JSON.parse(getCookie('data'));
    console.log("hello");
  }
  catch (SyntaxError)
  {
    console.log("error");
    data = {
      "developers" : [
          {
            "developer_id": 0,
            "project_id": 0,
            "name": "Petya"
          },
          {
            "developer_id": 1,
            "project_id": 0,
            "name": "Vasya"
          }
      ],
      "projects" : [
        {
          "project_id": 0,
          "customer_id": 0,
          "name": "Project"
        }
      ],
      "customers" : [
        {
          "customer_id": 0,
          "money": 1000,
          "name": "Company"
        }
      ],
      "bills" : [
        {
          "project_id": 0,
          "customer_id": 0,
          "amount": 1000,
          "paid": 10
        }
      ]
    }
  }
  formInfo.updateTable(data);
}

function createInput(name, placeholder) {
  const input = document.createElement("input");
  input.type = "text";
  input.name = name;
  input.placeholder = placeholder;
  return input;
}

function createSubmit(text) {
  const button = document.createElement("button");
  button.classList.add("btn");
  button.type = "submit";
  button.textContent = text;
  return button;
}

function createForm(formElement)   {
  if (!formInfo)
  {
    console.error("define formInfo");
    return;
  }
  if (!data) loadData();
  while (formElement.lastChild) {formElement.removeChild(formElement.lastChild);}
  for (const entry of formInfo.inputs)
  {
    const input = createInput(entry.name, entry.placeholder);
    formElement.appendChild(input);
  }
  const submit_button = createSubmit(formInfo.submit_button_text);
  formElement.appendChild(submit_button);
  formElement.onsubmit = onSubmit;
}

function onSubmit(event) {
  var new_datum = {};

  const form = event.target;
  for (const entry of formInfo.inputs)
  {
    let value = form.elements[entry.name].value;
    if (entry.is_numeric) {
      value = parseInt(value);
      if (isNaN(value)) {
        alert(entry.placeholder + " must have numeric value!");
        return false;
      }
    }
    if (entry.is_unique) {
      for (const datum of data[formInfo.table_name]) {
        if (datum[entry.name] == value) {
          alert(entry.placeholder + " must be unique");
          return false;
        }
      }
    }

    new_datum[entry.name] = value;
  }

  data[formInfo.table_name].push(new_datum);
  formInfo.updateTable(data);
  return false;
}
